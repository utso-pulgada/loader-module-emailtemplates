<?php


	namespace PDLoader;


	class Emailtemplates extends Loader{

		public function template($n,$data,$url=false){

			if( !$url ){
				if( $this->config() && isset($this->config()['url'] ) ){
					$url = $this->config()['url'];
				}else{
					$url = $this->url();
				}
			}

			$name = strtolower( str_replace(' ', '_', $n ) );

			if( file_exists(__DIR__.'/templates/'.$name ) ){

				$temp = file_get_contents(__DIR__.'/templates/'.$name );

				foreach( $data as $k => $v ){
					if( isset( $v['alias'] ) ){
						$temp = str_replace('{'.$v['alias'].'}',$v['contents'],$temp);
					}else{
						$temp = str_replace('{'.$k.'}',$v,$temp);
					}
				}

				$id = uniqid();

				$temp = str_replace('{version}', '?v='.$id, $temp);

				$temp = str_replace('{templateurl}', $url.'loader/modules/Emailtemplates/templates/'.$name.'/', $temp);

				$temp = str_replace('{templatebox}', $url.'loader/modules/Emailtemplates/templates/', $temp);

				return $temp; // return the template
			}

			return ''; // return empty if template not found 

		}

		public function onMenu(){
			return '<a href="/pdloader/loader{moduleurl}/preview.php" target="_blank" style="margin-left:12px">Preview</a>';
		}
		

		public function onBuild(){
			if( file_exists(__DIR__.'/package.json') ){
				unlink(__DIR__.'/package.json');
			}
			if( file_exists(__DIR__.'/package-lock.json') ){
				unlink(__DIR__.'/package-lock.json');
			}	
			if( file_exists(__DIR__.'/dist/index.html') ){
				unlink(__DIR__.'/dist/index.html');
			}
			if( file_exists(__DIR__.'/index.html') ){
				unlink(__DIR__.'/index.html');
			}
			if( file_exists(__DIR__.'/.gitignore') ){
				unlink(__DIR__.'/.gitignore');
			}
			if( file_exists(__DIR__.'/src') ){
				$this->deleteFolder(__DIR__.'/src');
			}
			if( file_exists(__DIR__.'/.phpintel') ){
				$this->deleteFolder(__DIR__.'/.phpintel');
			}
			if( file_exists(__DIR__.'/.cache') ){
				$this->deleteFolder(__DIR__.'/.cache');
			}
			if( file_exists(__DIR__.'/.git') ){
				$this->deleteFolder(__DIR__.'/.git');
			}

		}
		private function deleteFolder($dir){
			if(file_exists($dir)){
				$it = new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS);
				$files = new \RecursiveIteratorIterator($it,
				             \RecursiveIteratorIterator::CHILD_FIRST);

				foreach($files as $file) {
					chmod($file->getRealPath(),0755);
				    if ($file->isDir()){
				        rmdir($file->getRealPath());
				    } else {
				        unlink($file->getRealPath());
				    }
				}
				rmdir($dir);
			}
			
		}	

	}