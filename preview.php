<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Templates Preview</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css">
	<style>	
		body{
			background:#FFF;
			padding-top: 64px;
			padding-bottom: 6px;
		}
		.box{
			padding: 12px 8px;
			width: 100%;
			border-radius: 5px;
			border: 1px solid #ccc;
			background: #FFF;
			text-align:center;
			height:100%;
			display:block;
			text-decoration: none;
			color:#242424;
		}
		.box:hover,.box:active,.box:visited{
			text-decoration: none !important;
		}
		.box:hover{
			background:#ccc;
			color:#242424;
		}
		.box h5{
			margin-bottom:4px;
		}
		.mb16{
			margin-bottom: 16px;
		}
		.mb24{
			margin-bottom: 24px;	
		}
		.mb32{
			margin-bottom: 32px;
		}
		.bg-secondary{
			background:#ccc;
		}
		.text-center{
			text-align: center;
		}
		.c-white{
			color:#FFF;
		}
	</style>
</head>
<body>
	<section>
		<div class="container">	
			<div class="row">	
				<?php if( isset( $_GET['id'] ) && $_GET['id'] != '' ){ ?>
				
				<?php foreach( glob(__DIR__.'/templates/'.$_GET['id'].'/*') as $t ) { ?>
				<?php $arr = explode( '/', str_replace('\\','/',$t) ); ?>
				<?php $name = end( $arr ); ?>
				<?php if( isset( pathinfo($t)['extension'] ) && pathinfo($t)['extension'] == 'html' ){ ?>
				<?php

					$content = explode('</head>',file_get_contents(__DIR__.'/templates/'.$_GET['id'].'/'.$name) )[1];
					$content = $alias = str_replace(['{templatebox}','{templateurl}','{version}'],'',$content);

					preg_match_all('/{(.*?)}/', $content, $matches);
					$alias = implode(', ',array_unique( array_map('strVal',$matches[1]) ) );
					// -- create preview
					if( !file_exists( __DIR__.'/templates/'.$_GET['id'].'/preview') ){
						mkdir( __DIR__.'/templates/'.$_GET['id'].'/preview',0755);
					}
					
					if( !file_exists( __DIR__.'/templates/'.$_GET['id'].'/preview/'.$name) ) {
					    $preview_html = str_replace('{templatebox}'.$_GET['id'].'/','../',file_get_contents(__DIR__.'/templates/'.$_GET['id'].'/'.$name));
    					file_put_contents('templates/'.$_GET['id'].'/preview/'.$name,$preview_html);   
					}
                    
					// -- end
					
				?>
				<div class="col-12 mb32">
						<a href="preview.php">Back to templates</a>
				</div>
				<div class="col-12 mb32 bg-secondary">
					<h5 class="text-center c-white" style="padding:8px 0px 8px 0px;"><?php echo $name; ?></h5>
					<p class="text-center c-white">alias: <?php echo $alias; ?></p>
					<iframe src="<?php echo 'templates/'.$_GET['id'].'/preview/'.$name; ?>?v=<?php echo uniqid(); ?>" frameborder="0" height="600" style="width:100%"></iframe>
				</div>
				<?php } ?>
				<?php } ?>

				<?php }else{ ?>
				<?php foreach( glob(__DIR__.'/templates/*') as $t ){ ?>
				<?php $arr = explode( '/', str_replace('\\','/',$t) ); ?>
				<?php $name = end( $arr ); ?>
				<?php if( $name != 'images') { ?>
				<div class="col-12 col-md-3 mb16">
					<a href="?id=<?php echo $name ?>" class="box">
						<h5><?php echo $name?></h5>
						<?php $count = 0; ?>
						<?php foreach( glob($t.'/*') as $t2 ){ ?>
						<?php 
							
							if( isset( pathinfo($t2)['extension'] ) && pathinfo($t2)['extension'] == 'html' ){

								$count = $count+1;
							}
						?>
						<?php } ?>
						<p><?php echo $count; ?> templates(s)</p>
					</a>
				</div>
				<?php } ?>	
				<?php } ?>
				<?php } ?>
			</div>
		</div>
	</section>
</body>
</html>		