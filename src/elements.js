import droppable from './droppable.js'; // -- droppable
import updateElement from './updateElement.js';
import toolEvents from './events.js';

export default{
	tools(t){
		if( t == 'delete' ){
			return '<a class="delete-tool" href="#"><i class="ti-trash"></i></a>';
		}
		if( t == 'edit' ){
			return '<a class="edit-tool" href="#"><i class="ti-pencil"></i></a>';
		}
	},
	sidebarAccordions(){
		return `
		<div class="accordion">
		<div class="accordion-title"><h5>Containers, Wrappers</h5></div>
		<div class="accordion-body">
        ${this.toolList()}
		</div>
		</div>
		`;
	},
	toolList(){
		return [
			{
				id : 'element-section',
				label : 'Section',
				contents : '<section class="element droppable element-section edit-tool delete-tool with-tools"></section>',
				group : 'wrapper',
			},
			{
				id : 'element-div',
				label : 'Div',
				contents : '<div class="element droppable element-div edit-tool delete-tool with-tools"></div>',
				group : 'wrapper',
				html_template : true,
			},
			{
				id : 'element-paragraph',
				label : 'Paragraph',
				contents : '<div class="edit-tool delete-tool with-tools element element-paragraph"><p class="height-inherit" contentEditable="true"></p></div>',
				group : 'elements',
				html_template : true,
			},
			{
				id : 'element-table',
				label : 'Table',
				contents : '<div class="element element-table edit-tool delete-tool with-tools"><table class="table"><thead><tr><th contentEditable="true">Header 1</th><th contentEditable="true">Header 2</th></tr></thead><tbody><tr><td class="droppable"></td><td class="droppable"></td></tr></tbody></table></div>',
				group : 'components',
				html_template : true,
				activate_settings : ['columns-table']
			},
			{
				id : 'element-column',
				label : 'Column',
				contents : '<div class="container"><div class="element element-column with-tools edit-tool delete-tool row"><div class="col-item col-12 col-md-6 droppable"></div><div class="col-item col-12 col-md-6 droppable"></div></div></div>',
				group : 'components',
			 	activate_settings : ['columns']
			},
			{
				id : 'element-image',
				label : 'Image',
				contents : '<img src="" alt="Image" width="" height="" class="element element-image edit-tool delete-tool with-tools">',
				group : 'elements',
				html_template : true,
				activate_settings : ['img-src','width','height','img-alt','custom-class','custom-id','custom-css']
			},
			{
				id : 'element-header1',
				label : 'H1',
				contents : '<div class="element element-header1 delete-tool edit-tool with-tools"><h1 class="height-inherit" contentEditable="true"></h1></div>',
				group : 'header',
				html_template : true,
			},
			{
				id : 'element-header2',
				label : 'H2',
				contents : '<div class="element element-header2 delete-tool edit-tool with-tools"><h2 class="height-inherit" contentEditable="true"></h2></div>',
				group : 'header',
				html_template : true
			},
			{
				id : 'element-header3',
				label : 'H3',
				contents : '<div class="element element-header3 delete-tool edit-tool with-tools"><h3 class="height-inherit" contentEditable="true"></h3></div>',
				group : 'header',
				html_template : true
			},
			{
				id : 'element-header4',
				label : 'H4',
				contents : '<div class="element element-header4 delete-tool edit-tool with-tools"><h4 class="height-inherit" contentEditable="true"></h4</div>',
				group : 'header',
				html_template : true
			},
			{
				id : 'element-header5',
				label : 'H5',
				contents : '<div class="element element-header5 delete-tool edit-tool with-tools"><h5 class="height-inherit" contentEditable="true"></h5></div>',
				group : 'header',
				html_template : true
			},
			{
				id : 'element-header6',
				label : 'H6',
				contents : '<div class="element element-header6 delete-tool edit-tool with-tools"><h6 class="height-inherit" contentEditable="true"></h6></div>',
				group : 'header',
				html_template : true
			},
			{
				id : 'elements-html',
				label : 'HTML',
				contents : '<div class="element element-html edit-tool delete-tool with-tools"></div>',
				group : 'components',
				html_template : true,
				activate_settings : ['columns']
			},
			{
				id : 'elements-form',
				label : 'Form',
				contents : '<form action="" class="droppable element element-form delete-tool edit-tool with-tools" method="post"></form>',
				group : 'form',
			},
			{
				id : 'elements-input',
				label : 'Input',
				contents : '<input type="text" class="element element-input form-control delete-tool edit-tool with-tools" name="" placeholder="" value="">',
				group : 'form',
			},
			{
				id : 'elements-checkbox',
				label : 'Checkbox',
				contents : '<input type="checkbox" class="element element-checkbox delete-tool edit-tool with-tools" name="" value="">',
				group : 'form',
			},
			{
				id : 'elements-radio',
				label : 'Radio',
				contents : '<input type="radio" class="element element-radio delete-tool edit-tool with-tools" name="" value="">',
				group : 'form',
			},
			{
				id : 'elements-select',
				label : 'Select',
				contents : '<select class="element element-select form-control delete-tool edit-tool with-tools"></select>',
				group : 'form',
			},
			{
				id : 'elements-button',
				label : 'Button',
				contents : '<button class="element element-button btn btn-primay edit-tool delete-tool with-tools">Text</button>',
				group : 'form',
			}
		];
	},
	settingsOptions(t){
		const _self = this;

		let data = [
			{ 
				id : 'columns',
				label : 'Number of columns in row',
				type : 'number',
				value : '',
				placeholder : 'Number of columns in row...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns"]').value = document.querySelectorAll('.with-tools.tool-active .col-item').length;
					}
				},
				save : function(){
					if( document.querySelector('.with-tools.tool-active') ){

						let count = document.querySelector('.setting[data-settingsid="columns"]').value;

						let html = '';

						let contents_arr = [];

						document.querySelectorAll('.with-tools.tool-active .droppable').forEach(function(el){
							contents_arr.push(el.innerHTML);
						});

						let cols = [
							{ num : 1, class : 'col-item col-12 col-sm-12 col-md-12 col-lg-12 droppable' },
							{ num : 2, class : 'col-item col-12 col-sm-12 col-md-6 col-lg-6 droppable' },
							{ num : 3, class : 'col-item col-12 col-sm-12 col-md-4 col-lg-4 droppable' },
							{ num : 4, class : 'col-item col-12 col-sm-12 col-md-3 col-lg-3 droppable' },
							{ num : 5, class : 'col-item col-12 col-sm-12 col-md-2 col-lg-2 droppable' },
							{ num : 6, class : 'col-item col-12 col-sm-12 col-md-2 col-lg-2 droppable' }
						];

						for( var i = 0; i < parseInt( count ); i++ ){
							var contents = ( contents_arr[i] ) ? contents_arr[i] : '';

							var classes = ( cols.findIndex( i => i.num == count ) != -1 ) ? cols.find( i => i.num == count ).class : cols[0].class;
							html+=`<div class="${classes}">${contents}</div>`;

						}

						document.querySelector('.with-tools.tool-active').innerHTML = html;

						updateElement.updateElement();

						toolEvents.toolEvents(document.querySelector('.with-tools.tool-active').closest('.droppable'));
						
						droppable(document.querySelector('.with-tools.tool-active').closest('.element').querySelectorAll('.droppable'));
						
						updateElement.updateElement();

					}

				}

			},
			{ 
				id : 'structure',
				label : 'structure',
				type : 'textarea',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="structure"]') &&  document.querySelectorAll('.with-tools.tool-active') ){

						let html  = ( document.createElement('div') ).appendChild( document.querySelector('.with-tools.tool-active').cloneNode(true) );

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="structure"]').value = html;
						updateElement.updateElement();
					}
				},
				save : function(){

				}

			},
			{ 
				id : 'height',
				label : 'height (px/%)',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]') && document.querySelector('.with-tools.tool-active') ){
						let height = ( document.querySelector('.with-tools.tool-active').getAttribute('height') ) ? document.querySelector('.with-tools.tool-active').getAttribute('height')+'px' : '';
						let styleHeight = ( document.querySelector('.with-tools.tool-active').style.height != '' ) ? document.querySelector('.with-tools.tool-active').style.height : false;
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]').value = ( styleHeight ) ? styleHeight : height;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]') && document.querySelector('.with-tools.tool-active') ){
						let height = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="height"]').value;
						if( document.querySelector('.with-tools.tool-active').classList.contains('.element-image') ){
							document.querySelector('.with-tools.tool-active').setAttribute('height',parceInt(height));
						}

						document.querySelector('.with-tools.tool-active').style.minHeight = 'initial';
						document.querySelector('.with-tools.tool-active').style.height = height;
						updateElement.updateElement();
					}
				}

			},
			{ 
				id : 'width',
				label : 'Width (px/%)',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]') && document.querySelector('.with-tools.tool-active') ){
						let width = ( document.querySelector('.with-tools.tool-active').getAttribute('width') ) ? document.querySelector('.with-tools.tool-active').getAttribute('width')+'px' : '';
						let stylewidth = ( document.querySelector('.with-tools.tool-active').style.width != '' ) ? document.querySelector('.with-tools.tool-active').style.width : false;
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]').value = ( stylewidth ) ? stylewidth : width;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]') && document.querySelector('.with-tools.tool-active') ){
						let width = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="width"]').value;
						if( document.querySelector('.with-tools.tool-active').classList.contains('.element-image') ){
							document.querySelector('.with-tools.tool-active').setAttribute('width',parceInt(width));
						}

						document.querySelector('.with-tools.tool-active').style.minHeight = 'initial';
						document.querySelector('.with-tools.tool-active').style.width = width;
						updateElement.updateElement();
					}
				}

			},
			{ 
				id : 'img-alt',
				label : 'Alternative',
				type : 'input',
				value : '',
				placeholder : '',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]').value = ( ( document.querySelector('.with-tools.tool-active').getAttribute('alt') ) ? document.querySelector('.with-tools.tool-active').getAttribute('alt') : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active').setAttribute('alt', document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-alt"]').value);
						updateElement.updateElement();
					}
				}

			},
			{ 
				id : 'img-src',
				label : 'Image Source',
				type : 'input',
				value : '',
				placeholder : 'image source...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]').value = ( ( document.querySelector('.with-tools.tool-active').getAttribute('src') ) ? document.querySelector('.with-tools.tool-active').getAttribute('src') : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active').setAttribute('src', document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="img-src"]').value);
						updateElement.updateElement();
					}
				}

			},
			{ 
				id : 'columns-table',
				label : 'Number of columns in row',
				type : 'number',
				value : '',
				placeholder : 'Number of columns in row...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]').value =  document.querySelectorAll('.with-tools.tool-active th').length;
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="columns-table"]') && document.querySelector('.with-tools.tool-active') ){
						

						let count = document.querySelector('.setting[data-settingsid="columns-table"]').value;

						let html = '';
						let html2 = '';

						let contents_arr = [];

						document.querySelectorAll('.with-tools.tool-active thead tr').forEach(function(el){
							let el2 = '<tr>'
							for( var i=0;i<count;i++){
								if( el.querySelectorAll('th')[i] ){
									let tdel = document.createElement('div');
										tdel.appendChild( el.querySelectorAll('th')[i].cloneNode(true) );
									el2+=tdel.innerHTML;
								}else{
									el2+='<th class="droppable"></th>';
								}
							}
							el2+='</tr>';
							html+=el2;

						});


						document.querySelectorAll('.with-tools.tool-active tbody tr').forEach(function(el){
							let el2 = '<tr>'
							for( var i=0;i<count;i++){
								if( el.querySelectorAll('td')[i] ){
									let tdel = document.createElement('div');
										tdel.appendChild( el.querySelectorAll('td')[i].cloneNode(true) );
									el2+=tdel.innerHTML;
								}else{
									el2+='<td class="droppable"></td>';
								}
							}
							el2+='</tr>';
							html2+=el2;
						});

						// -- append new contents for table header and table body
						document.querySelector('.with-tools.tool-active thead').innerHTML = html;
						document.querySelector('.with-tools.tool-active tbody').innerHTML = html2;

						updateElement.updateElement();

						toolEvents.toolEvents(document.querySelector('.with-tools.tool-active').closest('.droppable'));
						
						droppable(document.querySelector('.with-tools.tool-active').closest('.element').querySelectorAll('.droppable'));

					
					}
				}

			},
			{ 
				id : 'custom-id',
				label : 'Custom ID',
				type : 'input',
				value : '',
				placeholder : 'Custom ID...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]').value =  ( document.querySelector('.with-tools.tool-active').getAttribute('id') ) ? document.querySelector('.with-tools.tool-active').getAttribute('id') : '';
					}
				},
				save : function(){

				}
			},
			{ 
				id : 'custom-class',
				label : 'Custom Classes',
				type : 'input',
				value : '',
				placeholder : 'Custom classes...',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]') && document.querySelector('.with-tools.tool-active') ){
						let classes =  ( document.querySelector('.with-tools.tool-active').getAttribute('class') ) ? document.querySelector('.with-tools.tool-active').getAttribute('class') : '';

						classes = classes.replace('element',''); // -- remove element class
						classes = classes.replace('tool-active','');
						classes = classes.replace('active','');
						classes = classes.replace('droppable','');
						classes = classes.replace('ui-droppable','');
						classes = classes.replace('edit-tool','');
						classes = classes.replace('delete-tool','');
						classes = classes.replace('with-tools','');

						if( document.querySelector('.with-tools.tool-active').classList.contains('element-column') ){
							classes = classes.replace('container-fluid','');
							classes = classes.replace('container','');
						}

						_self.toolList().forEach(function(item){
							classes = classes.replace(item.id,'');
						});

						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]').value = classes.trim();
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]') && document.querySelector('.with-tools.tool-active') ){
						
						let classes = [];
						let default_classes = [];

						if(  document.querySelector('.with-tools.tool-active').classList.contains('element') ){
							default_classes.push('element');
						}
						if(  document.querySelector('.with-tools.tool-active').classList.contains('tool-active') ){
							default_classes.push('tool-active');
						}
						if(  document.querySelector('.with-tools.tool-active').classList.contains('droppable') ){
							default_classes.push('droppable');
						}

						if(  document.querySelector('.with-tools.tool-active').classList.contains('ui-droppable') ){
							default_classes.push('ui-droppable');
						}

						if(  document.querySelector('.with-tools.tool-active').classList.contains('edit-tool') ){
							default_classes.push('edit-tool');
						}

						if(  document.querySelector('.with-tools.tool-active').classList.contains('delete-tool') ){
							default_classes.push('delete-tool');
						}

						if(  document.querySelector('.with-tools.tool-active').classList.contains('with-tools') ){
							default_classes.push('with-tools');
						}


						for( var i = 0; i < _self.toolList().length;i++){
							if ( document.querySelector('.with-tools.tool-active').classList.contains(_self.toolList()[i].id) ){
								default_classes.push(_self.toolList()[i].id);
								break;
							}
						}

						let classes2 = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-class"]').value.split(' ');

						classes2.forEach(function(item){
							classes.push(item.trim());
						});

						if( document.querySelector('.with-tools.tool-active').getAttribute('class') ){
							document.querySelector('.with-tools.tool-active').getAttribute('class').split(' ').forEach(function( item ){
								if( default_classes.indexOf(item) == -1 ){
									document.querySelector('.with-tools.tool-active').classList.remove(item);								
								}
							});
						}

						classes.forEach(function(item){
							document.querySelector('.with-tools.tool-active').classList.add(item);
						});

					}
				}
			},
			{ 
				id : 'custom-type',
				label : 'Type',
				type : 'select',
				value : 'section',
				wrapper : '<div class="col-12 col-md-4 col-lg-6" style="margin-bottom:8px">',
				options : [
					{
						value : 'section',
						text : 'Section',
						selected : true,
					},
					{
						value : 'div',
						text : 'Div',
						selected : false
					}
				],
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active').setAttribute('id',document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-id"]').value);
					}
				}
			},
			{ 
				id : 'custom-width',
				label : 'Container width',
				type : 'radio',
				wrapper : '<div class="col-12 col-md-6" style="margin-bottom:8px">',
				options : [
					{ 
						text : 'Full',
						value : 'full',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : true
					},
					{ 
						text : 'Fixed',
						value : 'fixed',
						wrapper : '<div class="col-12 col-md-3" style="margin-bottom:8px">',
						selected : false
					}
				],
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"]') && document.querySelector('.with-tools.tool-active') ){
						let v = ( ( document.querySelector('.with-tools.tool-active').classList.contains('container') ) ? 'fixed' : 'full' );
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"][value="'+v+'"]').checked = true;
					}
				},
				save : function(){
					if( document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active').style.width = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"][checked]').value;
					}
				}
			},
			{ 
				id : 'custom-css',
				label : 'Custom CSS',
				type : 'textarea',
				value : '',
				placeholder : 'Custom css...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-css"]').value = ( ( document.querySelector('.with-tools.tool-active').getAttribute('style') ) ? document.querySelector('.with-tools.tool-active').getAttribute('style') : '' );
					}
				},
				save : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('.with-tools.tool-active').style.width = document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-width"][checked]').value;
					}	
				}
			},
			{ 
				id : 'custom-js',
				label : 'Custom JS',
				type : 'textarea',
				value : '',
				placeholder : 'Custom js...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-js"]').value = document.querySelector('#customjs').innerHTML;
					}
				},
				save : function(){

				}
			},
			{ 
				id : 'custom-html',
				label : 'Custom html',
				type : 'textarea',
				value : '',
				placeholder : 'Custom html...',
				wrapper : '<div class="col-12 col-md-12" style="margin-bottom:8px">',
				onedit : function(){
					if( document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]') && document.querySelector('.with-tools.tool-active') ){
						document.querySelector('#editor-modal .editor-modal-body .setting[data-settingsid="custom-html"]').value = document.querySelector('.with-tools.tool-active').innerHTML;
					}
				},
				save : function(){

				}
			}
		];

		return ( data.findIndex( i => i.id == t ) != -1 ) ? data.find( i => i.id == t ) : false;
	}
}