import PouchDB from 'pouchdb-browser';
import elements from './elements.js'; // -- html interface
import makeid from './makeid.js' // -- create random hash module
import elementsParser from './elementsParser.js';
import $ from './external/jquery/jquery.js';
import droppable from './droppable.js'; // -- droppable
import toolEvents from './events.js'; // -- module events

const entities = require("entities");

let db = new PouchDB('websiteeditor');

export default{
	templateDroppable(){
		droppable( document.querySelector('.editor-body.droppable') );
	},
	toolSidebar(){
		let el = document.createElement('div');
			el.classList.add('editor-tool');
			el.innerHTML = '<a href="#" class="editor-tool-trigger"><i class="ti-plus"></i></a><a href="#" class="editor-tool-settings" style=" top: 65px; width: 40px; height: 40px; right: -65px;"><i class="ti-settings"></i></a><div class="editor-tool-body"><div style="overflow-y:auto;overflow-x:hidden;height:100%;padding:0px 24px;">'+elementsParser.sidebarAccordions()+'</div></div>';
			el.querySelector('.editor-tool-trigger').addEventListener('click',function(){
				if( this.closest('.editor-tool').classList.contains('active') ){
					this.closest('.editor-tool').classList.remove('active');
				}else{
					this.closest('.editor-tool').classList.add('active');
				}
			});

			el.querySelector('.editor-tool-settings').addEventListener('click',function(){
				elementsParser.settings(['custom-css','custom-js']);
			});


		document.querySelector('body').appendChild(el);

		$( ".draggable" ).draggable({
			start : function(event,ui){
				event.target.style.background = '#ededed';
			  	if( document.querySelector('.editor-tool.active') ){
					document.querySelector('.editor-tool.active').classList.remove('active');
				}
			},
			cursor: "crosshair",
			helper: "clone"
		});
	},
	tool( t ){
		const _self = this;

		let el = $('.droppable.active');


		if( el.length == 0 ){ // if not found then abort
			console.log('no active item found');
			return;
		}


		if( elements.toolList().findIndex( i => i.id == t ) == -1 ){
			return;
		}


		let html = ( elements.toolList().findIndex( i => i.id == t ) != -1 ) ? elements.toolList().find( i => i.id == t ).contents : '';
		let template_id = $('#template-editor').attr('data-templateid');
		let edit_tool = elements.tools('edit');
		let delete_tool = elements.tools('delete');

		db.get( template_id ).then(function(doc){
			let elementid = 'element-'+makeid(6)+doc.contents.length;
			
			el.append( html );
			el.find('.element:last-child').attr('data-elementid',elementid);

			let raw_html = el.html();

			let contents = _self.buildContents();

			toolEvents.toolEvents(document.querySelector('.droppable.active'));

			droppable(document.querySelector('.droppable.active'));

			return db.put({
				_id : doc._id,
				_rev : doc._rev,
				templateid : doc.templateid,
				contents : contents,
				created_at : doc.created_at,
				updated_at : ( ( new Date() ).getTime() ).toString()
			}).then(function(){
				// trigger columns settings
				if( elements.toolList().findIndex( i => i.id == t ) != -1 && Array.isArray( elements.toolList().find( i => i.id == t ).activate_settings ) ){
					document.querySelector('.element[data-elementid="'+elementid+'"]').classList.add('tool-active');
					elementsParser.settings(elements.toolList().find( i => i.id == t ).activate_settings);
				}

				db.get( doc.templateid ).then(function(doc){
					$('#template-output').html(entities.decodeHTML(doc.contents));
				});
			});
		}).catch(function(err){
			console.log(err);
			console.log('Unable to retrieve template '+template_id);
		});
	},
	buildContents(){
		let el = document.createElement('div');
		el.innerHTML = document.querySelector('.editor-body').innerHTML;
		el.querySelectorAll('.tools').forEach(function(item){
			item.remove();
		});

		return entities.encodeHTML(el.innerHTML);

	}


	
}